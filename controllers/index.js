let authController = require("./authController");
let userController = require("./userController");

module.exports = { userController, authController };
