let { userService } = require("../services");
const jwt = require("jsonwebtoken");
const accessTokenSecret = "0lm8UQXOO4J501PqE4XQCo8MwQX70kr3";

module.exports = {
  async signUp(req, res) {
    console.log("in user controller");

    let createdUser = await userService.signUp(
      req.body.username,
      req.body.password,
      req.body.role,
      req.body.first_name
    );
    console.log("inside userController, createdUser: ", createdUser);
    const accessToken = jwt.sign(
      {
        id: createdUser.id,
        username: createdUser.username,
        role: createdUser.role,
      },
      accessTokenSecret
    );
    return res.status(200).json({
      user: {
        username: createdUser.username,
        id: createdUser.id,
      },
      token: accessToken,
    });
  },
};
