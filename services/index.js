let authService = require("./authService");
let userService = require("./userService");

module.exports = {
  authService,
  userService,
};
