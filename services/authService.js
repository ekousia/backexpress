let { sequelize } = require("../sequelize");

module.exports = {
  async login(submittedUsername, submittedPassword) {
    let user;
    console.log("in service");
    try {
      console.log("saved username", submittedUsername);
      user = await sequelize.models.User.findOne({
        where: {
          username: submittedUsername,
        },
      });
      if (user) {
        let isPasswordCorrect = await user.verifyPassword(submittedPassword);
        console.log("password is correct:", isPasswordCorrect);
        console.log("user:", user);
      }
    } catch (error) {
      console.log(error);
    }
    console.log("return");
    return user;
  },
};
