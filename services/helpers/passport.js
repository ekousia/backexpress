// var bCrypt = require("bcrypt");
// var passport = require("passport");

// module.exports = {
//   async signUp(auth) {
//     var Auth = auth;
//     console.log("inside passport file", Auth);
//     var LocalStrategy = require("passport-local").Strategy;
//     passport.use(
//       "local-signup",
//       new LocalStrategy(
//         {
//           usernameField: "username",
//           passwordField: "password",
//           passReqToCallback: true, // allows us to pass back the entire request to the callback
//         },
//         function (req, username, password, done) {
//           console.log("!!!!!!!! Signup for - ", username);
//           var generateHash = function (password) {
//             console.log("generateHash function");
//             return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
//           };
//           Auth.findOne({
//             where: {
//               username: username,
//             },
//           }).then(function (user) {
//             //console.log(user);
//             console.log("inside findOne");
//             if (user) {
//               return done(null, false, {
//                 message: "That username is already taken",
//               });
//             } else {
//               var userPassword = generateHash(password);
//               var data = {
//                 username: username,
//                 password: userPassword,
//                 firstname: req.body.firstname,
//                 role: req.body.role,
//               };

//               Auth.create(data).then(function (newUser, created) {
//                 console.log("inside create");
//                 if (!newUser) {
//                   return done(null, false);
//                 }
//                 if (newUser) {
//                   return done(null, newUser);
//                 }
//               });
//             }
//           });
//         }
//       )
//     );

//     //LOCAL SIGNIN
//     passport.use(
//       "local-signin",
//       new LocalStrategy(
//         {
//           // by default, local strategy uses username and password, we will override with username

//           usernameField: "username",

//           passwordField: "password",

//           passReqToCallback: true, // allows us to pass back the entire request to the callback
//         },

//         function (req, username, password, done) {
//           var Auth = auth;

//           var isValidPassword = function (userpass, password) {
//             return bCrypt.compareSync(password, userpass);
//           };
//           console.log("logged to", username);
//           Auth.findOne({
//             where: {
//               username: username,
//             },
//           })
//             .then(function (user) {
//               console.log(user);
//               if (!user) {
//                 return done(null, false, {
//                   message: "Username does not exist",
//                 });
//               }

//               if (!isValidPassword(user.password, password)) {
//                 return done(null, false, {
//                   message: "Incorrect password.",
//                 });
//               }

//               var userinfo = user.get();
//               return done(null, userinfo);
//             })
//             .catch(function (err) {
//               console.log("Error:", err);

//               return done(null, false, {
//                 message: "Something went wrong with your Signin",
//               });
//             });
//         }
//       )
//     );

//     //serialize
//     passport.serializeUser(function (auth, done) {
//       done(null, auth.id);
//     });

//     // deserialize user
//     passport.deserializeUser(function (id, done) {
//       Auth.findById(id).then(function (user) {
//         if (user) {
//           done(null, user.get());
//         } else {
//           done(user.errors, null);
//         }
//       });
//     });
//   },
// };
