let { sequelize } = require("../sequelize");

module.exports = {
  async signUp(
    currentUsername,
    currentPassword,
    currentRole,
    currentFirstName
  ) {
    let result;
    let user;
    let hashedPassword;
    console.log("in service");
    try {
      user = await sequelize.models.User.findOne({
        where: { username: currentUsername },
      });
      if (user === null) {
        console.log("before hashed password", currentPassword);
        hashedPassword = await sequelize.models.User.createPassword(
          currentPassword
        );
        console.log("after hashed password", hashedPassword);
        result = await sequelize.models.User.create({
          username: currentUsername,
          password: hashedPassword,
          role: currentRole,
          firstname: currentFirstName,
        });
        user = await sequelize.models.User.findOne({
          where: { username: currentUsername },
        });
        console.log("inside userService, user: ", user.dataValues);
        return user.dataValues;
      } else {
        console.log("Please try with a different username"); // true
      }
    } catch (error) {
      console.log(error);
    }
  },
};
