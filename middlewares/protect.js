const jwt = require("jsonwebtoken");
const accessTokenSecret = "0lm8UQXOO4J501PqE4XQCo8MwQX70kr3";
let { sequelize } = require("../sequelize");

const verifyToken = (token) => {
  return new Promise((resolve, reject) => {
    console.log("!!!!!!!!!!!! in verify token");
    jwt.verify(token, accessTokenSecret, (err, user) => {
      if (err) reject(err);
      resolve(user);
    });
  });
};

exports.protect = async function (req, res, next) {
  try {
    let authHeader = req.headers["authorization"];
    let token = authHeader && authHeader.split(" ")[1];
    console.log(null);
    console.log("null");
    // const bearer = req.headers.authorization;
    // if (!bearer || !bearer.startsWith("Bearer ")) {
    //   return res.status(401).end();
    // }
    // const token = bearer.split("Bearer ")[1].trim();

    if (token && token !== "null") {
      console.log("token exists: ", token);
      let user = await verifyToken(token);
      let dbUser = await sequelize.models.User.findByPk(user.id);
      if (dbUser) {
        req.user = dbUser;
        return next();
      }
    } else {
      console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! token doesn't exist");
      return res.status(400).json({ message: "no token found" });
    }
  } catch (error) {
    console.log("error: ", error);
    res.status(401).json({ message: "user is not authenticated" });
  }
};
