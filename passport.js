let passport = require("passport");
const { v4: uuidv4 } = require("uuid");
const session = require("express-session");
let { Strategy: LocalStrategy } = require("passport-local");
let { sequelize } = require("./sequelize");

exports.initPassport = function (app) {
  app.use(
    session({
      secret: "17R7Imp13BBjOYLODqHg1Db3CIGXpFo9",
      saveUninitialized: false,
      resave: false,
      cookie: {
        maxAge: 24 * 60 * 60 * 1000, // 24h
        httpOnly: false,
        secure: false,
      },
      //maxAge: 24 * 60 * 60 * 1000,
      name: "mySession",
      genid: (req) => {
        return uuidv4(); // use UUIDs for session IDs
      },
    })
  );
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser(function (user, done) {
    console.log("serialize user id:", user.id);
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    console.log("DESIERIALIZE:", id);
    sequelize.models.User.findByPk(id)
      .then((user) => {
        if (user) {
          console.log("DESIERIALIZE:", user.username);
          done(null, user);
        }
      })
      .catch((err) => {
        done(err, null);
      });
    // try {
    //   console.log("deserialize user id:", id);
    //   let user = await sequelize.models.User.findByPk(id);
    //   if (!user) {
    //     return done(new Error("user not found"));
    //   }
    //   done(null, user);
    // } catch (e) {
    //   done(e);
    // }
  });

  passport.use(
    new LocalStrategy(async function (username, password, done) {
      let user;
      try {
        user = await sequelize.models.User.findOne({
          where: { username: username },
        });
        if (!user) {
          console.log("!user");
          return done(null, false, { message: "Incorrect username." });
        }
        let passwordMatches = await user.verifyPassword(password);
        if (!passwordMatches) {
          console.log("!password");
          return done(null, false, { message: "Incorrect password." });
        }
        console.log("user found", user.username);
        return done(null, user);
      } catch (err) {
        console.error("error", err);
        done(err);
      }
    })
  );

  console.log("Passport Init.");
};
