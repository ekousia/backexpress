const { Sequelize, DataTypes, Model } = require("sequelize");
let { initUser, User } = require("./models/userModel");
let { initCat, Cat } = require("./models/catModel");
const sequelize = new Sequelize("express", "root", "0000", {
  host: "localhost",
  dialect: "mariadb",
});

let startDB = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
    // await User.sync({ force: true });
    // console.log("The table for the User model was just (re)created!");

    await initUser(sequelize);
    await initCat(sequelize);
    User.hasMany(Cat, {
      foreignKey: {
        name: "userId",
      },
    });
    Cat.belongsTo(User, {
      as: "Owner",
      foreignKey: {
        name: "userId",
      },
    });
    await sequelize.sync({ force: false });
    console.log("All models were synchronized successfully.");
    // sequelize.models.User.hasMany(sequelize.models.Cat);
    // sequelize.models.Cat.belongsTo(sequelize.models.User);
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

exports.sequelize = sequelize;
exports.startDB = startDB;
