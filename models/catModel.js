const { Sequelize, DataTypes, Model } = require("sequelize");
const User = require("./userModel");

class Cat extends Model {
  // static associate() {
  //   Cat.belongsTo(User, { foreignKey: "id" });
  // }
}

exports.initCat = async function (sequelize) {
  Cat.init(
    {
      // Model attributes are defined here
      catId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      catName: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      // id: {
      //   type: DataTypes.INTEGER,
      //   allowNull: false,
      // },
    },

    {
      sequelize, // We need to pass the connection instance
      modelName: "Cat", // We need to choose the model name
      tableName: "Cat",
    }
  );

  //User.sync();
  console.log("Cat model created.");
};

exports.Cat = Cat;
