const { Sequelize, DataTypes, Model } = require("sequelize");
const bcrypt = require("bcrypt");
//const Cat = require("./catModel");

class User extends Model {
  static async createPassword(password) {
    let salt = await bcrypt.genSalt(8);
    let hashedPassword = await bcrypt.hash(password, salt);
    return hashedPassword;
  }
  verifyPassword(submittedPassword) {
    console.log("submitted password: ", submittedPassword);
    return bcrypt.compare(submittedPassword, this.password);
  }
  //  User.hasMany(Cat);
}

exports.initUser = async function (sequelize) {
  User.init(
    {
      // Model attributes are defined here
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      firstname: {
        type: DataTypes.STRING,
        //allowNull: false,
      },
      password: DataTypes.STRING,
      role: {
        type: DataTypes.STRING,
      },
    },

    {
      sequelize, // We need to pass the connection instance
      modelName: "User", // We need to choose the model name
      tableName: "User",
    }
  );
  //User.sync();+

  console.log("User model created.");
};

exports.User = User;
