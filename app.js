var express = require("express");
var cors = require("cors");
let { initRouter } = require("./routes");
let { initPassport } = require("./passport");
let { startDB, sequelize } = require("./sequelize");
// const session = require("express-session");
let cookieParser = require("cookie-parser");
// const jwt = require('jsonwebtoken');
const bodyParser = require("body-parser");
// const accessTokenSecret = 'youraccesstokensecret';
// require("./routes/index");

startDB();

var app = express();
let logToConsole = (req, res, next) => {
  console.log(
    `${req.method.toUpperCase()} request was made to: ${req.originalUrl}`
  );
  console.log("hostname: ", req.hostname);
  //console.log("sess: ", req.session && req.session.id);
  next();
};
app.use(logToConsole);
// app.use(cors("*"));

app.use(
  cors({
    // allowHeaders: ['Content-type', 'Authorization', 'Session', 'Origin'],
    // methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
    // origin: /$/,
    credentials: true,
    origin: true,
  })
);

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:8080");

  res.header("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(bodyParser.json());
// app.use(
//   session({
//     secret: "17R7Imp13BBjOYLODqHg1Db3CIGXpFo9",
//     saveUninitialized: false,
//     resave: false,
//     cookie: {
//       maxAge: 24 * 60 * 60 * 1000, // 24h
//       httpOnly: false,
//       secure: false,
//     },
//     //maxAge: 24 * 60 * 60 * 1000,
//     name: "mySession",
//   })
// );
// app.use(passport.initialize());
// app.use(passport.session());

// passport.serializeUser(function (user, done) {
//   console.log("serialize user id:", user.id);
//   done(null, user.id);
// });

// passport.deserializeUser(async (id, done) => {
//   try {
//     console.log("deserialize user id:", id);
//     let user = await sequelize.models.User.findByPk(id);
//     if (!user) {
//       return done(new Error("user not found"));
//     }
//     done(null, user);
//   } catch (e) {
//     done(e);
//   }
// });

// passport.use(
//   new LocalStrategy(async function (username, password, done) {
//     let user;
//     try {
//       user = await sequelize.models.User.findOne({
//         where: { username: username },
//       });
//       if (!user) {
//         console.log("!user");
//         return done(null, false, { message: "Incorrect username." });
//       }
//       let passwordMatches = await user.verifyPassword(password);
//       if (!passwordMatches) {
//         console.log("!password");
//         return done(null, false, { message: "Incorrect password." });
//       }
//       console.log("user found", user.username);
//       return done(null, user);
//     } catch (err) {
//       console.error("error", err);
//       done(err);
//     }
//   })
// );

async function start() {
  initPassport(app);
  await initRouter(app);
  // await initPassport(app);

  app.listen(3002, () => {
    console.log(`Example app listening at http://localhost:${3002}`);
  });
}

start();
module.exports = app;
