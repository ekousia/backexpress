var express = require("express");
var router = express.Router();
let authRouter = require("./auth");
let userRouter = require("./user");
let initRouter = async (app) => {
  console.log("initRouter start!!!!");
  app.use("/auth", authRouter);
  app.use("/user", userRouter);

  app.get("/", function (req, res, next) {
    //  res.render('index', { title: 'Express' });
    console.log("request", req.body);
    return res.status(200).send("Hello");
  });
};

// module.exports = router;

exports.initRouter = initRouter;
