var express = require("express");
var router = express.Router();
//let passport = require("passport");
let { userController } = require("../controllers");

router.post("/signup", userController.signUp);

module.exports = router;
