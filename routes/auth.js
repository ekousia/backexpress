var express = require("express");
var router = express.Router();
let { authController } = require("../controllers");
let passport = require("passport");
const jwt = require("jsonwebtoken");
const accessTokenSecret = "0lm8UQXOO4J501PqE4XQCo8MwQX70kr3";
let { sequelize } = require("../sequelize");
let { protect } = require("../middlewares");

// const bodyParser = require('body-parser');

// router.post("/login", authController.login);

// const isAuth = function (req, res, next) {
//   // console.log(req);
//   // console.log();
//   console.log("inside isAuth function. session:", req.sessionID);
//   if (req.isAuthenticated()) {
//     // if (req.user) {
//     console.log("user isAuthenticated");
//     return next();
//   } else {
//     console.log("user not authenticated");
//     res.status(401).json({ message: "user not authenticated" });
//   }
// };

// const isAuth2 = (req, res, next) => {
//   passport.authenticate("local");
//   next();
// };

//   ******************* New --- Passport  ************************   //
router.post("/login", async function (req, res, next) {
  let user;
  try {
    user = await sequelize.models.User.findOne({
      where: { username: req.body.username },
    });
    if (!user) {
      console.log("!user");
      return res
        .status(400)
        .json({ message: "Incorrect username or password." });
    }
    let passwordMatches = await user.verifyPassword(req.body.password);
    if (!passwordMatches) {
      console.log("!password");
      return res
        .status(400)
        .json({ message: "Incorrect username or password." });
    }
    console.log("user found", user.username);
    const accessToken = jwt.sign(
      { id: user.id, username: user.username, role: user.role },
      accessTokenSecret
    );
    console.log("2222222222222222 user:", user.username);
    console.log("11111111111accessToken: ", accessToken);
    return res.status(200).json({
      user: {
        username: user.username,
        id: user.id,
      },
      token: accessToken,
    });
  } catch (err) {
    console.error("error", err);
    return res.status(500).json({ message: "error" });
  }
});

router.get("/me", protect, (req, res, next) => {
  if (req.user) {
    console.log("user is already authenticated");
    return res.status(200).json({ user: req.user });
  }
  return res.status(400).end();
});

//router.get("/me", passport.authenticate("local"), isAuth);

router.get("/logout", (req, res, next) => {
  // console.log("222222222222logout request, accessToken", accessToken);
  // req.accessToken = null;
  // console.log("after logout");

  console.log("kkkkkkkkkkkkkkkkk headers:", req.headers["authorization"]);
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  console.log("@@@@@@@@@@@@@@ token:", token);
  if (token && token !== "null") {
    jwt.verify(token, accessTokenSecret, async (err, user) => {
      if (err) {
        return res.status(403).end();
      }
      console.log("outside if user.id", user);
      let dbUser = await sequelize.models.User.findByPk(user.id);
      console.log("dbUser", dbUser.username);

      //res.user = dbUser;
      //next();
      res.status(200).json({
        user: null,
        token: null,
      });
    });
  } else {
    res.status(401).json({ message: "user is not authenticated" });
  }
});

router.post("/addCat", protect, async function (req, res, next) {
  console.log("cat:", req.body.cat);
  let cats;
  try {
    let newCat = await sequelize.models.Cat.create({
      catName: req.body.cat,
      userId: req.user.id,
    });
    const owner = await sequelize.models.User.findByPk(req.user.id);
    const hisCats = await owner.getCats();
    console.log(owner.toJSON());
    hisCats.map((cat) => {
      console.log(cat.toJSON());
      return cat;
    });
    console.log((await newCat.getOwner()).toJSON());

    const withcats = await sequelize.models.User.findByPk(req.user.id, {
      include: sequelize.models.Cat,
    });
    return res.status(200).json({ ownerWithCats: withcats });
    // cats = await sequelize.models.Cat.findOne({
    //   where: {
    //     id: req.body.id,
    //     catName: req.body.cat,
    //   },
    // });
    // if (!cats) {
    //   let result = await sequelize.models.Cat.create({
    //     catName: req.body.cat,
    //     id: req.body.id,
    //   });

    //   return res.status(200).json({ cat: result.catName });
    // } else {
    //   return res
    //     .status(401)
    //     .json({ message: "this cat has already been added to your account" });
    // }
  } catch (err) {
    console.log(err);
  } finally {
    console.log("finally");
  }
});

router.post("/allCats", async function (req, res, next) {
  let cats;
  try {
    cats = await sequelize.models.Cat.findAll({
      include: [
        {
          association: "id",
          attributes: ["username: req.body.username"],
        },
      ],
    });
    if (cats) {
      return res.status(200).json({ cats: cats });
    } else {
      let message = "you haven't any cats";
      return res.status(200).json({ cats: message });
    }
  } catch (err) {
    console.log(err);
  } finally {
    console.log("finally");
  }
});

// router.post("/login", function (req, res, next) {
//   //let { username, password } = req.body;
//   return passport.authenticate("local", (err, user, info) => {
//     console.log("info:", info);
//     if (err) return next(err);
//     if (!user) {
//       return res.json(403, {
//         message: "no user found",
//       });
//     }

//     // Manually establish the session...
//     console.log("user:", user.username);
//     req.logIn(user, function (err) {
//       if (err) return next(err);
//       console.log("session:", req.sessionID);
//       console.log("passport: ", req.session);
//       return res.status(200).cookie("custom", req.session).json({
//         message: "user is authenticated with login",
//       });
//     });
//   })(req, res, next);
// });

module.exports = router;
